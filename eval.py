import gym
from tqdm import tqdm
from sys import stdout
from os import getcwd

xml_file_base = getcwd() + "/eval_envs/"
xml_file_name = {
    'Flat Plane': 'hexapod_flat.xml',
    'Tilted Plane': 'hexapod_tilted.xml',
    'Sequence': 'hexapod_sequence.xml',
    'Height Map Geom': 'hexapod_heightmap.xml',
}

terrains = ['Flat Plane', 'Tilted Plane', 'Sequence', 'Height Map Geom']
policies = ['PPO', 'SAC', 'B-TLe']

REPEATS = 100
TIMESTEPS = 1000
display_bar = True

for terrain in terrains:
    env = gym.make("hexapod:Hexapod-v0", xml_file=xml_file_base + xml_file_name[terrain], healthy_z_range=(-100, 100))
    for policy in policies:
        if policy == "PPO":
            from stable_baselines3 import PPO
            model = PPO.load('ppo_baseline_model.zip')
        if policy == "SAC":
            from stable_baselines3 import SAC
            model = SAC.load('sac_baseline_model.zip')
        if policy == "B-TLe":
            from MyPPO import PPO

        distance_list = []
        energy_list = []
        total_energy = 0
        total_distance = 0
        if display_bar:
            bar = tqdm(desc="Evaluation", ncols=200, file=stdout, total=REPEATS)
        for k in range(REPEATS):
            if display_bar:
                bar.update()

            if policy == "B-TLe":
                model = PPO.load('b-tle_model_trained.zip')

            observation = env.reset()
            prev_observation = observation

            distance = 0
            energy = 0
            for t in range(TIMESTEPS):
                action, _state = model.predict(observation, deterministic=True)
                observation, reward, done, info = env.step(action)
                env.render()

                energy = energy + (abs(action) * abs(observation[:18]-prev_observation[:18])).sum()
                prev_observation = observation

            distance_list.append(info['y_position'])
            energy_list.append(energy)

            total_energy = total_energy + energy
            total_distance = total_distance + info['y_position']

        if display_bar:
            bar.close()
        print(terrain, "-", policy, "-", k + 1, "Repeats")
        print(distance_list)
        print(energy_list)
        print("Mean Distance = ", total_distance / REPEATS)
        print("Mean Energy = ", total_energy / REPEATS)
