import gym
from MyPPO import PPO
from args import trained_model_file, eval_env
from os import getcwd

eval_env_dict = {
    'flat': '/eval_envs/hexapod_flat.xml',
    'tilted': '/eval_envs/hexapod_tilted.xml',
    'sequence': '/eval_envs/hexapod_sequence.xml',
    'heightmap': '/eval_envs/hexapod_heightmap.xml',
}

env = gym.make("hexapod:Hexapod-v0", xml_file=getcwd() + eval_env_dict[eval_env], reset_noise_scale=0.0)

model = PPO.load(trained_model_file)

observation = env.reset()
env.render()

for t in range(1000):
    action, _state = model.predict(observation, deterministic=True)
    observation, reward, done, info = env.step(action)
    env.render()
