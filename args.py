#arguments for b-tle_train.py
log = False
algorithm_verbose = 1  # default = 1
num_experts = 9
num_envs = 8
model_file = "b-tle_model.zip"

# arguments for b-tle_test.py
eval_env = 'flat'  # [flat, tilted, sequence, heightmap]
trained_model_file = "b-tle_model_trained.zip"
print_predicted_tilt = False
