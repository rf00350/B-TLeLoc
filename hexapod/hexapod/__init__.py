from gym.envs.registration import register

register(
    id="Hexapod-v0",
    entry_point="hexapod.envs:HexapodEnv",
    max_episode_steps=1000,
    reward_threshold=6000.0,
)
