import numpy as np
from gym import utils
from gym.envs.mujoco import mujoco_env
from math import tan, pi


DEFAULT_CAMERA_CONFIG = {
    "distance": 20.0,
}


class HexapodEnv(mujoco_env.MujocoEnv, utils.EzPickle):
    def __init__(
        self,
        xml_file="assets/hexapod.xml",
        ctrl_cost_weight=0.1,
        diff_cost_weight=0.1,
        prev_action = np.zeros(18),
        healthy_reward=0,
        terminate_when_unhealthy=True,
        healthy_z_range=(0.6, 4),
        reset_noise_scale=0.1,
        exclude_current_positions_from_observation=True,
        plane_tilt=0,
    ):
        utils.EzPickle.__init__(**locals())
        
        self._ctrl_cost_weight = ctrl_cost_weight
        self._diff_cost_weight = diff_cost_weight
        self._prev_action = prev_action
        self._plane_tilt = plane_tilt

        self._healthy_reward = healthy_reward
        self._terminate_when_unhealthy = terminate_when_unhealthy
        self._healthy_z_range = healthy_z_range


        self._reset_noise_scale = reset_noise_scale

        self._exclude_current_positions_from_observation = (
            exclude_current_positions_from_observation
        )

        mujoco_env.MujocoEnv.__init__(self, xml_file, 5)

    @property
    def healthy_reward(self):
        return (
            float(self.is_healthy or self._terminate_when_unhealthy)
            * self._healthy_reward
        )

    def control_cost(self, action):
        control_cost = self._ctrl_cost_weight * np.sum(np.square(action))
        return control_cost
         
    def differential_cost(self, prev_action, action):
        differential_cost = self._diff_cost_weight * np.sum(np.square(np.subtract(action, prev_action)))
        return differential_cost

    @property
    def is_healthy(self):
        state = self.state_vector()
        min_z, max_z = self._healthy_z_range
        is_healthy = np.isfinite(state).all() and min_z <= state[2]-(tan(self._plane_tilt * pi / 180)*(self.get_body_com("torso")[:2].copy()[1])) <= max_z
        return is_healthy

    @property
    def done(self):
        done = not self.is_healthy if self._terminate_when_unhealthy else False
        return done
        
    def give_tilt(self, tilt):
        self._plane_tilt = tilt

    def step(self, action):
        xy_position_before = self.get_body_com("torso")[:2].copy()
        self.do_simulation(action, self.frame_skip)
        xy_position_after = self.get_body_com("torso")[:2].copy()

        xy_velocity = (xy_position_after - xy_position_before) / self.dt
        x_velocity, y_velocity = xy_velocity

        ctrl_cost = self.control_cost(action)
        diff_cost = self.differential_cost(self._prev_action, action)
        self._prev_action = action

        forward_reward = y_velocity
        healthy_reward = self.healthy_reward

        rewards = forward_reward + healthy_reward
        costs = ctrl_cost + diff_cost

        reward = rewards - costs
        done = self.done
        observation = self._get_obs()
        info = {
            "reward_forward": forward_reward,
            "reward_ctrl": -ctrl_cost,
            "reward_diff": -diff_cost,
            "reward_survive": healthy_reward,
            "x_position": xy_position_after[0],
            "y_position": xy_position_after[1],
            "distance_from_origin": np.linalg.norm(xy_position_after, ord=2),
            "x_velocity": x_velocity,
            "y_velocity": y_velocity,
            "forward_reward": forward_reward,
        }

        return observation, reward, done, info

    def _get_obs(self):
        position = self.sim.data.qpos.flat.copy()
        velocity = self.sim.data.qvel.flat.copy()

        if self._exclude_current_positions_from_observation:
        
        ################################
            position = position[7:]
            velocity = velocity[6:]
        ################################
        
        observations = np.concatenate((position, velocity))
        return observations

    def reset_model(self):
        noise_low = -self._reset_noise_scale
        noise_high = self._reset_noise_scale

        qpos = self.init_qpos + self.np_random.uniform(
            low=noise_low, high=noise_high, size=self.model.nq
        )
        qvel = self.init_qvel + self._reset_noise_scale * self.np_random.randn(
            self.model.nv
        )
        self.set_state(qpos, qvel)

        observation = self._get_obs()

        return observation

    def viewer_setup(self):
        for key, value in DEFAULT_CAMERA_CONFIG.items():
            if isinstance(value, np.ndarray):
                getattr(self.viewer.cam, key)[:] = value
            else:
                setattr(self.viewer.cam, key, value)
