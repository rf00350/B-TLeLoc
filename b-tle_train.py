import gym
from MyPPO import PPO
from MyActorCriticPolicy import MultiExpertActorCriticPolicy
from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.callbacks import BaseCallback
import wandb
from wandb.integration.sb3 import WandbCallback
import numpy as np
import random
import os
from args import log, algorithm_verbose, num_experts, num_envs, model_file


class MyLogCallback(BaseCallback):
    def _on_step(self) -> bool:
        self.logger.record("info/forward_reward", np.mean([d['reward_forward'] for d in env.buf_infos]))
        self.logger.record("info/ctrl_cost", np.mean([d['reward_ctrl'] for d in env.buf_infos]))
        self.logger.record("info/diff_cost", np.mean([d['reward_diff'] for d in env.buf_infos]))
        self.logger.record("info/y_position", np.mean([d['y_position'] for d in env.buf_infos]))
        self.logger.record("info/distance_from_origin", np.mean([d['distance_from_origin'] for d in env.buf_infos]))
        self.logger.record("reward", np.mean(env.buf_rews))
        return True

if log:
    run = wandb.init(
        project="Training",
        name="b-tle_model",
        sync_tensorboard=True,
    )

for iterations in range(10000):
    tilt = random.choice([-20, -15, -10, -5, 0, 5, 10, 15, 20])
    ctrl_cost_weight = 0.01
    diff_cost_weight = 0.1

    if os.path.exists(model_file):
        first_time = False
    else:
        print("First")
        first_time = True

    print("Now Training: ", tilt, " degrees")

    env_kwargs = {"ctrl_cost_weight": ctrl_cost_weight, "diff_cost_weight": diff_cost_weight, "plane_tilt": tilt, "xml_file": os.getcwd() + "/training_envs/hexapod_" + str(tilt) + ".xml"}

    log_callback = MyLogCallback()

    env = make_vec_env("hexapod:Hexapod-v0", n_envs=num_envs, env_kwargs=env_kwargs)
    policy_kwargs = dict(features_in=env.observation_space.shape[0],
                         features_mid=50,
                         features_out=64,
                         tilt=tilt,
                         num_envs=env.num_envs,
                         num_experts=num_experts,
                         )

    if first_time:
        if log:
            model = PPO(policy=MultiExpertActorCriticPolicy, env=env, policy_kwargs=policy_kwargs, verbose=algorithm_verbose, tensorboard_log=f"runs/{run.id}")
        else:
            model = PPO(policy=MultiExpertActorCriticPolicy, env=env, policy_kwargs=policy_kwargs, verbose=algorithm_verbose)
        first_time = False
    else:
        if log:
            model = PPO.load(model_file, tensorboard_log=f"runs/{run.id}")
            print("Loaded")
        else:
            model = PPO.load(model_file)
            print("Loaded")
        model.set_env(env)

    model.policy.ground_truth_tilt = tilt
    model.learn(total_timesteps=1_000, progress_bar=False)

    print("Iteration: ", iterations, ", Plane Tilt: ", tilt, " degrees Completed")

    model.save(model_file)
