# B-TLe Loc

## Installation instructions
Install Anaconda3 if you haven't done so already
```
wget -q https://repo.anaconda.com/archive/Anaconda3-2022.10-Linux-x86_64.sh
bash Anaconda3-2022.10-Linux-x86_64.sh
```

Install MuJoCo 2.1.0
```
mkdir .mujoco
cd .mujoco
wget -q https://mujoco.org/download/mujoco210-linux-x86_64.tar.gz
tar -xf mujoco210-linux-x86_64.tar.gz
cd ..
```

Clone the B-TLeLoc git repo
```
git clone https://gitlab.surrey.ac.uk/rf00350/B-TLeLoc.git
```

Build the b-tle conda environment and install the custom hexapod package
```
cd B-TLeLoc
conda env create -f requirements.yml
conda activate b-tle
conda develop hexapod
```

Add the following lines to your .bashrc (replace $USERNAME with your username)
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/$USERNAME/.mujoco/mujoco210/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/nvidia
export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libGLEW.so
```

## Running instructions
To render the model on a test environment
```
python b-tle_test.py
```

To run the numerical evaluation script
```
python eval.py
```

To run the model training script
```
python b-tle_train.py
```
