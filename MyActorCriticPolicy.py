from stable_baselines3.common.policies import BasePolicy, ActorCriticPolicy
import torch.nn as nn
from stable_baselines3.common.torch_layers import (
    FlattenExtractor,
    BaseFeaturesExtractor,
    NatureCNN,
    MlpExtractor
)
import torch as th
import gym
from stable_baselines3.common.type_aliases import Schedule
from typing import Optional, List, Union, Dict, Type, Any, Tuple

from stable_baselines3.common.distributions import (
    BernoulliDistribution,
    CategoricalDistribution,
    DiagGaussianDistribution,
    MultiCategoricalDistribution,
    StateDependentNoiseDistribution,
    Distribution,
    make_proba_distribution,
)

import numpy as np
from functools import partial

from args import print_predicted_tilt

device = "cuda" if th.cuda.is_available() else "mps" if th.backends.mps.is_available() else "cpu"


class RNN(nn.Module):
    def __init__(self, input_size, num_envs, hidden_size, output_size):
        super(RNN, self).__init__()

        self.hidden_size = hidden_size
        self.num_envs = num_envs
        self.i2h = nn.Sequential(
            nn.Linear(input_size + hidden_size, 64),
            nn.ReLU(),
            nn.Linear(64, hidden_size),
        )
        self.i2o = nn.Sequential(
            nn.Linear(input_size + hidden_size, 64),
            nn.ReLU(),
            nn.Linear(64, output_size),
        )

    def forward(self, input_tensor, hidden_tensor):
        combined = th.cat((input_tensor, hidden_tensor), 1)

        hidden = self.i2h(combined)
        output = self.i2o(combined)
        return output, hidden

    def init_hidden(self):
        return th.zeros(self.num_envs, self.hidden_size, device=device, requires_grad=True)


class MultiExpertActorCriticPolicy(ActorCriticPolicy):
    def __init__(
        self,
        observation_space: gym.spaces.Space,
        action_space: gym.spaces.Space,
        lr_schedule: Schedule,
        net_arch: Optional[List[Union[int, Dict[str, List[int]]]]] = None,
        activation_fn: Type[nn.Module] = nn.Tanh,
        ortho_init: bool = True,
        use_sde: bool = False,
        log_std_init: float = 0.0,
        full_std: bool = True,
        sde_net_arch: Optional[List[int]] = None,
        use_expln: bool = False,
        squash_output: bool = False,
        features_extractor_class: Type[BaseFeaturesExtractor] = FlattenExtractor,
        features_extractor_kwargs: Optional[Dict[str, Any]] = None,
        normalize_images: bool = True,
        optimizer_class: Type[th.optim.Optimizer] = th.optim.Adam,
        optimizer_kwargs: Optional[Dict[str, Any]] = None,
        features_in: int = 0,
        features_mid: int = 0,
        features_out: int = 0,
        tilt: int = 0,
        num_envs: int = 0,
        num_experts: int = 0,
    ):
        #super().__init__(observation_space, action_space, lr_schedule, net_arch, activation_fn, ortho_init, use_sde,
        #                 log_std_init, full_std, sde_net_arch, use_expln, squash_output, features_extractor_class,
        #                 features_extractor_kwargs, normalize_images, optimizer_class, optimizer_kwargs)

        if optimizer_kwargs is None:
            optimizer_kwargs = {}
            # Small values to avoid NaN in Adam optimizer
            if optimizer_class == th.optim.Adam:
                optimizer_kwargs["eps"] = 1e-5

        BasePolicy.__init__(
            self,
            observation_space,
            action_space,
            features_extractor_class,
            features_extractor_kwargs,
            optimizer_class=optimizer_class,
            optimizer_kwargs=optimizer_kwargs,
            squash_output=squash_output,
        )

        # Default network architecture, from stable-baselines
        if net_arch is None:
            if features_extractor_class == NatureCNN:
                net_arch = []
            else:
                net_arch = [dict(pi=[64, 64], vf=[64, 64])]

        self.net_arch = net_arch
        self.activation_fn = activation_fn
        self.ortho_init = ortho_init

        self.features_extractor = features_extractor_class(self.observation_space, **self.features_extractor_kwargs)
        self.features_dim = self.features_extractor.features_dim

        self.normalize_images = normalize_images
        self.log_std_init = log_std_init
        dist_kwargs = None
        # Keyword arguments for gSDE distribution
        if use_sde:
            dist_kwargs = {
                "full_std": full_std,
                "squash_output": squash_output,
                "use_expln": use_expln,
                "learn_features": False,
            }

        if sde_net_arch is not None:
            warnings.warn("sde_net_arch is deprecated and will be removed in SB3 v2.4.0.", DeprecationWarning)

        self.use_sde = use_sde
        self.dist_kwargs = dist_kwargs

        # Action distribution

        self.action_dist_ensemble = []

        for i in range(num_experts):
            self.action_dist_ensemble.append(make_proba_distribution(action_space, use_sde=use_sde, dist_kwargs=dist_kwargs))

        self._build(lr_schedule, features_in, features_mid, features_out, tilt, num_envs, num_experts)

    def _build_mlp_extractor(self, num_experts: int) -> None:
        """
        Create the policy and value networks.
        Part of the layers can be shared.
        """
        # Note: If net_arch is None and some features extractor is used,
        #       net_arch here is an empty list and mlp_extractor does not
        #       really contain any layers (acts like an identity module).

        self.mlp_extractor_ensemble = nn.ModuleList()

        for i in range(num_experts):
            self.mlp_extractor_ensemble.append(MlpExtractor(
            self.features_dim,
            net_arch=self.net_arch,
            activation_fn=self.activation_fn,
            device=self.device,
            ))

    def _build(self, lr_schedule: Schedule, features_in: int, features_mid: int, features_out: int, tilt: int, num_envs: int, num_experts: int) -> None:
        """
        Create the networks and the optimizer.

        :param lr_schedule: Learning rate schedule
            lr_schedule(1) is the initial learning rate
        """
        self._build_mlp_extractor(num_experts)

        latent_dim_pi = self.mlp_extractor_ensemble[0].latent_dim_pi

        self.action_net_ensemble = nn.ModuleList()
        self.log_std_ensemble = nn.ParameterList()
        self.value_net_ensemble = nn.ModuleList()

        for i in range(num_experts):
            if isinstance(self.action_dist_ensemble[i], DiagGaussianDistribution):
                action_net, log_std = self.action_dist_ensemble[i].proba_distribution_net(
                    latent_dim=latent_dim_pi, log_std_init=self.log_std_init
                )
            elif isinstance(self.action_dist_ensemble[i], StateDependentNoiseDistribution):
                action_net, log_std = self.action_dist_ensemble[i].proba_distribution_net(
                    latent_dim=latent_dim_pi, latent_sde_dim=latent_dim_pi, log_std_init=self.log_std_init
                )
            elif isinstance(self.action_dist_ensemble[i],
                            (CategoricalDistribution, MultiCategoricalDistribution, BernoulliDistribution)):
                action_net = action_dist_ensemble[i].proba_distribution_net(latent_dim=latent_dim_pi)
            else:
                raise NotImplementedError(f"Unsupported distribution '{self.action_dist}'.")

            value_net = nn.Linear(self.mlp_extractor_ensemble[i].latent_dim_vf, 1)

            self.action_net_ensemble.append(action_net)
            self.log_std_ensemble.append(log_std)
            self.value_net_ensemble.append(value_net)

        # Init weights: use orthogonal initialization
        # with small initial weight for the output
        if self.ortho_init:
            # TODO: check for features_extractor
            # Values from stable-baselines.
            # features_extractor/mlp values are
            # originally from openai/baselines (default gains/init_scales).

            self.features_extractor.apply(partial(self.init_weights, gain=np.sqrt(2)))

            for i in range(num_experts):
                module_gains = {
                    # self.features_extractor: np.sqrt(2),  # Implemented above
                    self.mlp_extractor_ensemble[i]: np.sqrt(2),
                    self.action_net_ensemble[i]: 0.01,
                    self.value_net_ensemble[i]: 1,
                }
                for module, gain in module_gains.items():
                    module.apply(partial(self.init_weights, gain=gain))

        # Setup optimizer with initial learning rate

        self.ground_truth_tilt = tilt
        self.num_envs = num_envs
        self.num_experts = num_experts
        self.features_out = features_out
        self.attention = th.zeros(self.num_experts, device=device)
        self.rnn = RNN(36, num_envs, 128, self.num_experts)
        self.soft = nn.Softmax(dim=1)
        self.rnn_loss_fn = nn.CrossEntropyLoss()
        # self.rnn_optimizer = th.optim.SGD(self.rnn.parameters(), lr=0.0001, momentum=0.9)  # remove rnn parameters from main optimizer if used
        # self.rnn_scheduler = th.optim.lr_scheduler.ReduceLROnPlateau(self.rnn_optimizer, 'min', patience=50, threshold=0.02, verbose=True)
        self.rnn_hidden = self.rnn.init_hidden()
        self.idx_to_class = [-20, -15, -10, -5, 0, 5, 10, 15, 20]
        self.class_to_idx = {"-20": 0, "-15": 1, "-10": 2, "-5": 3, "0": 4, "5": 5, "10": 6, "15": 7, "20": 8}
        # self.rnn_labels = th.zeros(num_envs, dtype=th.int64, device=device)
        self.rollout_data_rnn_hidden = th.zeros(64, 128, device=device)
        self.deployment_rnn_hidden = th.zeros(1, 128, device=device)
        # self.rnn_loss = 0
        # self.rnn_counter = 0
        # self.rnn_output = th.zeros(num_envs, num_experts, device=device, requires_grad=True)

        self.optimizer = self.optimizer_class(self.parameters(), lr=lr_schedule(1), **self.optimizer_kwargs)

    def attn_func(self, tilt):
        self.attention[:] = 0
        if -20 <= tilt < -17:
            self.attention[0] = 1
        elif -17 <= tilt < -12:
            self.attention[1] = 1
        elif -12 <= tilt < -7:
            self.attention[2] = 1
        elif -7 <= tilt < -2:
            self.attention[3] = 1
        elif -2 <= tilt < 3:
            self.attention[4] = 1
        elif 3 <= tilt < 8:
            self.attention[5] = 1
        elif 8 <= tilt < 13:
            self.attention[6] = 1
        elif 13 <= tilt < 18:
            self.attention[7] = 1
        elif 18 <= tilt <= 20:
            self.attention[8] = 1
        return

    def vec_attn_func(self, vec_tilt):
        size = vec_tilt.size()[0]
        vec_attention = th.zeros(size, self.num_experts, device=device)
        for n in range(size):
            tilt = vec_tilt[n]
            self.attn_func(tilt)
            vec_attention[n] = self.attention
        return vec_attention

    def forward(self, obs: th.Tensor, deterministic: bool = False) -> Tuple[th.Tensor, th.Tensor, th.Tensor]:
        """
        Forward pass in all the networks (actor and critic)

        :param obs: Observation
        :param deterministic: Whether to sample or use deterministic actions
        :return: action, value and log probability of the action
        """
        # Preprocess the observation if needed
        features = self.extract_features(obs)

        rnn_output, self.rnn_hidden = self.rnn(features, self.rnn_hidden.detach())

        '''
        with torch.enable_grad():
        self.rnn.train()
        
        label = self.class_to_idx[str(self.ground_truth_tilt)]

        self.rnn_labels = torch.full((self.num_envs,), label, device=device)
        
        self.rnn_loss += self.rnn_loss_fn(rnn_output, self.rnn_labels)  # torch.Size([8, 9]) torch.Size([8])
        
        self.rnn_counter = self.rnn_counter + 1
        if self.rnn_counter == num_steps + 1:  # +1 for computing final time step
            self.rnn_counter = 0

            self.rnn_optimizer.zero_grad()
            self.rnn_loss.backward()  # calculate gradients
            self.rnn_optimizer.step()

            if log:
                wandb.log({"rnn loss": self.rnn_loss, "plane_tilt": self.ground_truth_tilt})

            self.rnn_hidden = self.rnn.init_hidden(device)
            self.rnn_loss = 0
        '''

        vec_attention = self.soft(rnn_output)

        values_ensemble = th.zeros((features.size()[0], 1, self.num_experts), device=device)
        actions_ensemble = th.zeros((features.size()[0], 18, self.num_experts), device=device)
        log_prob_ensemble = th.zeros((features.size()[0], self.num_experts), device=device)

        for i in range(self.num_experts):
            latent_pi, latent_vf = self.mlp_extractor_ensemble[i](features)

            values = self.value_net_ensemble[i](latent_vf)
            distribution = self._get_action_dist_from_latent(latent_pi, i)
            actions = distribution.get_actions(deterministic=deterministic)
            log_prob = distribution.log_prob(actions)
            actions = actions.reshape((-1,) + self.action_space.shape)

            values_ensemble[:, :, i] = values
            actions_ensemble[:, :, i] = actions
            log_prob_ensemble[:, i] = log_prob

        actions_out = th.matmul(actions_ensemble, vec_attention.unsqueeze(2)).squeeze(2)
        values_out = th.matmul(values_ensemble, vec_attention.unsqueeze(2)).squeeze(2)
        log_prob_out = th.matmul(log_prob_ensemble.unsqueeze(1), vec_attention.unsqueeze(2)).squeeze()

        return actions_out, values_out, log_prob_out

    def _get_action_dist_from_latent(self, latent_pi: th.Tensor, i: int) -> Distribution:
        """
        Retrieve action distribution given the latent codes.

        :param latent_pi: Latent code for the actor
        :return: Action distribution
        """
        mean_actions = self.action_net_ensemble[i](latent_pi)

        if isinstance(self.action_dist_ensemble[i], DiagGaussianDistribution):
            return self.action_dist_ensemble[i].proba_distribution(mean_actions, self.log_std_ensemble[i])
        elif isinstance(self.action_dist_ensemble[i], CategoricalDistribution):
            # Here mean_actions are the logits before the softmax
            return self.action_dist_ensemble[i].proba_distribution(action_logits=mean_actions)
        elif isinstance(self.action_dist_ensemble[i], MultiCategoricalDistribution):
            # Here mean_actions are the flattened logits
            return self.action_dist_ensemble[i].proba_distribution(action_logits=mean_actions)
        elif isinstance(self.action_dist_ensemble[i], BernoulliDistribution):
            # Here mean_actions are the logits (before rounding to get the binary actions)
            return self.action_dist_ensemble[i].proba_distribution(action_logits=mean_actions)
        elif isinstance(self.action_dist_ensemble[i], StateDependentNoiseDistribution):
            return self.action_dist_ensemble[i].proba_distribution(mean_actions, self.log_std_ensemble[i], latent_pi)
        else:
            raise ValueError("Invalid action distribution")

    def _predict(self, observation: th.Tensor, deterministic: bool = False) -> th.Tensor:
        """
        Get the action according to the policy for a given observation.

        :param observation:
        :param deterministic: Whether to use stochastic or deterministic actions
        :return: Taken action according to the policy
        """
        features = self.extract_features(observation)

        rnn_output, self.deployment_rnn_hidden = self.rnn(features, self.deployment_rnn_hidden)
        if print_predicted_tilt: print("Predicted Tilt =", self.idx_to_class[th.argmax(rnn_output).item()])

        vec_attention = self.soft(rnn_output)

        actions_ensemble = th.zeros((features.size()[0], 18, self.num_experts), device=device)

        for i in range(self.num_experts):
            latent_pi = self.mlp_extractor_ensemble[i].forward_actor(features)

            distribution = self._get_action_dist_from_latent(latent_pi, i)
            actions = distribution.get_actions(deterministic=deterministic)
            actions = actions.reshape((-1,) + self.action_space.shape)

            actions_ensemble[:, :, i] = actions

        actions_out = th.matmul(actions_ensemble, vec_attention.unsqueeze(2)).squeeze(2)

        return actions_out

    def get_distribution(self, obs: th.Tensor) -> Distribution:
        # get_distribution() SHOULD NOT GET CALLED
        """
        Get the current policy distribution given the observations.

        :param obs:
        :return: the action distribution.
        """
        sys.exit()

    def evaluate_actions(self, obs: th.Tensor, actions: th.Tensor) -> Tuple[th.Tensor, th.Tensor, th.Tensor]:
        """
        Evaluate actions according to the current policy,
        given the observations.

        :param obs:
        :param actions:
        :return: estimated value, log likelihood of taking those actions
            and entropy of the action distribution.
        """
        # Preprocess the observation if needed
        features = self.extract_features(obs)

        rnn_output, _ = self.rnn(features, self.rollout_data_rnn_hidden)

        vec_attention = self.soft(rnn_output)

        values_ensemble = th.zeros((features.size()[0], 1, self.num_experts), device=device)
        distribution_entropy_ensemble = th.zeros((features.size()[0], self.num_experts), device=device)
        log_prob_ensemble = th.zeros((features.size()[0], self.num_experts), device=device)

        for i in range(self.num_experts):
            latent_pi, latent_vf = self.mlp_extractor_ensemble[i](features)

            values = self.value_net_ensemble[i](latent_vf)
            distribution = self._get_action_dist_from_latent(latent_pi, i)
            log_prob = distribution.log_prob(actions)

            values_ensemble[:, :, i] = values
            distribution_entropy_ensemble[:, i] = distribution.entropy()
            log_prob_ensemble[:, i] = log_prob

        values_out = th.matmul(values_ensemble, vec_attention.unsqueeze(2)).squeeze(2)
        log_prob_out = th.matmul(log_prob_ensemble.unsqueeze(1), vec_attention.unsqueeze(2)).squeeze()
        distribution_entropy_out = th.matmul(distribution_entropy_ensemble.unsqueeze(1), vec_attention.unsqueeze(2)).squeeze()

        return values_out, log_prob_out, distribution_entropy_out

    def predict_values(self, obs: th.Tensor) -> th.Tensor:
        """
        Get the estimated values according to the current policy given the observations.

        :param obs:
        :return: the estimated values.
        """
        features = self.extract_features(obs)

        if features.size()[0] == 8:
            rnn_output, self.rnn_hidden = self.rnn(features, self.rnn_hidden.detach())

        if features.size()[0] == 1:
            rnn_output, _ = self.rnn(features, th.zeros(1, 128, device=device))

        vec_attention = self.soft(rnn_output)

        values_ensemble = th.zeros((features.size()[0], 1, self.num_experts), device=device)

        for i in range(self.num_experts):
            latent_vf = self.mlp_extractor_ensemble[i].forward_critic(features)

            values = self.value_net_ensemble[i](latent_vf)

            values_ensemble[:, :, i] = values

            values_out = th.matmul(values_ensemble, vec_attention.unsqueeze(2)).squeeze(2)

        return values_out
